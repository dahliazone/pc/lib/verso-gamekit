#ifndef VERSO_3D_VERSO_3D_COMMON_HPP
#define VERSO_3D_VERSO_3D_COMMON_HPP


#if defined(WIN32) || defined _WIN32 || defined __CYGWIN__
#	if defined(VERSO_3D_BUILD_DYNAMIC)
#		ifdef __GNUC__
#			define VERSO_3D_API __attribute__ ((dllexport))
#		else
#			define VERSO_3D_API __declspec(dllexport) // Note: actually gcc seems to also supports this syntax.
#			define VERSO_3D_EXPIMP
#		endif
#	else
#		ifdef __GNUC__
#			define VERSO_3D_API __attribute__ ((dllimport))
#		else
#			define VERSO_3D_API __declspec(dllimport) // Note: actually gcc seems to also supports this syntax.
#			define VERSO_3D_EXPIMP extern
#		endif
#	endif
#	define VERSO_3D_PRIVATE
#else
#	if __GNUC__ >= 4
#		define VERSO_3D_API __attribute__ ((visibility ("default")))
#		define VERSO_3D_PRIVATE  __attribute__ ((visibility ("hidden")))
#	else
#		define VERSO_3D_API
#		define VERSO_3D_PRIVATE
#	endif
#endif


#endif // End header guard

