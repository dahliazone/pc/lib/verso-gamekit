#!/bin/bash

echo "verso-gamekit: Fetching externals from repositories..."

if [ ! -d ../verso-3d ]; then
        git clone https://gitlab.com/dahliazone/pc/lib/verso-3d.git ../verso-3d
else
        pushd ../verso-3d
        git pull --rebase
        popd
fi

echo "verso-3d: Resursing into..."
pushd ../verso-3d
./fetch_parent_externals.sh
popd

echo "verso-gamekit: All done"

