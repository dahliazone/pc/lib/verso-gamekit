echo "verso-gamekit: Fetching externals from repositories..."

IF NOT EXIST ..\verso-3d (
	git clone https://gitlab.com/dahliazone/pc/lib/verso-3d.git ..\verso-3d
) else (
	pushd ..\verso-3d
	git pull --rebase
	popd
)

echo "verso-3d: Resursing into..."
pushd ..\verso-3d
call fetch_parent_externals.cmd
popd

echo "verso-gamekit: All done"
